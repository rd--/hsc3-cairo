hsc3-cairo
----------

[haskell](http://haskell.org/)
[supercollider](http://audiosynth.com/)
([hsc3](http://rohandrape.net/?t=hsc3))
[cairo](http://cairographics.org/)
drawing

© [rohan drape](http://rohandrape.net/),
  2012-2022,
  [gpl](http://gnu.org/copyleft/)
