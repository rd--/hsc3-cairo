-- | Buffer display.  <http://sccode.org/1-1HR> (f0)
module Sound.Sc3.Cairo.Scope.Bd where

import qualified Graphics.Rendering.Cairo as C {- cairo -}

import Sound.Sc3 {- hsc3 -}
import qualified Sound.Sc3.Cairo.Scope.Shell as Shell {- hsc3-cairo -}

-- * Type

type R = Double

data Bd_Type =
  Bd_Line | Bd_Warp | Bd_Flower
  deriving (Show)

data Bd =
  Bd {gain :: R
     ,theta :: R
     ,theta_incr :: R
     ,trails :: R
     ,version :: Bd_Type}
  deriving (Show)

bd_default :: Bd
bd_default = Bd {gain = 0.5, theta = 0, theta_incr = 0, trails = 1, version = Bd_Line}

-- * Render

rotate_about :: R -> R -> R -> C.Render ()
rotate_about a x y = do
  C.translate x y
  C.rotate a
  C.translate (-x) (-y)

render_line :: R -> R -> [R] -> C.Render ()
render_line n a d = do
  C.translate 0 (n/2)
  let f (x,y) = let y' = (y * n * a)
                in if x == 0 then C.moveTo x y' else C.lineTo x y'
  mapM_ f (zip [0..] d)
  C.stroke

pt_rotate :: Floating a => a -> (a,a) -> (a,a)
pt_rotate a (x,y) =
    let s = sin a
        c = cos a
    in (x * c - y * s,y * c + x * s)

render_warp :: R -> R -> [R] -> C.Render ()
render_warp n a d = do
  C.translate (n/2) (n/2)
  let f (x,y) = let (x',y') = pt_rotate (y * 2 * pi) (x * a,y * a)
                in if x == 0 then C.moveTo x' y' else C.lineTo x' y'
  mapM_ f (zip [0..] d)
  C.stroke

render_flower :: R -> R -> [R] -> C.Render ()
render_flower n t d = do
  let d0 = d !! 0
  C.translate (n/2) (n/2)
  C.moveTo (d0 * d0) 0
  let f (x,y) = let a = ((x `modE` n) / n) * 2 * pi + t
                    (x',y') = pt_rotate a (y * y,x * y)
                in C.lineTo x' y'
  mapM_ f (zip [0..] d)
  C.stroke

bd_render :: Shell.Render_f Bd
bd_render sh bd = do
  let n = fromIntegral (Shell.sh_nf sh)
      a = gain bd
      t = theta bd
      v = version bd
      r = case v of
            Bd_Flower -> return ()
            _ -> rotate_about t (n/2) (n/2)
  C.rectangle 0 0 n n
  C.setSourceRGBA 0 0 0 (trails bd)
  C.fill
  C.setSourceRGBA 0 1 0 1
  let f d = case version bd of
              Bd_Line -> render_line n a d
              Bd_Warp -> render_warp n a d
              Bd_Flower -> render_flower n t d
      g d = C.save >> r >> f d >> C.restore
  mapM_ g (Shell.sh_data sh)
  return (bd {theta = t + theta_incr bd})

-- * Key

bd_key :: Shell.Key_f Bd
bd_key k bd =
    case k of
      'T' -> bd {trails = trails bd + 0.025}
      't' -> bd {trails = trails bd - 0.025}
      'S' -> bd {theta_incr = theta_incr bd + 0.025}
      's' -> bd {theta_incr = theta_incr bd - 0.025}
      'w' -> bd {version = Bd_Warp}
      'l' -> bd {version = Bd_Line}
      'f' -> bd {version = Bd_Flower}
      'z' -> bd {theta = 0,theta_incr = 0}
      _ -> bd

bd_shell :: Int -> Shell.Shell Bd
bd_shell nc = Shell.sh_default_mon nc bd_render bd_key
