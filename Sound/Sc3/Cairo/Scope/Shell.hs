-- | Shell
module Sound.Sc3.Cairo.Scope.Shell where

import Control.Monad.IO.Class {- transformers -}
import Data.IORef {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Data.List.Split {- split -}
import qualified Data.Text as T {- text -}
import qualified Graphics.Rendering.Cairo as C {- cairo -}
import qualified Graphics.UI.Gtk as G {- gtk3 -}
import Sound.Osc.Fd {- hosc -}
import Sound.Sc3.Fd {- hsc3 -}

-- * Types

type Render_f st = Shell st -> st -> C.Render st
type Interact_f st = Udp -> Shell st -> st -> IO st
type Bracket_f st = (Interact_f st,Interact_f st)
type Sc3_f st = Interact_f st
type Key_f st = Char -> st -> st
data Shell st = Shell {sh_mon :: Maybe (Int,Int) -- ^ Group (protected) and bus index
                      ,sh_nc :: Int -- ^ Number of channels
                      ,sh_nf :: Int -- ^ Number of frames
                      ,sh_b :: Int -- ^ Buffer ID
                      ,sh_data :: [[Double]] -- ^ Signal data
                      ,sh_bracket :: Bracket_f st
                      ,sh_sc3 :: Sc3_f st
                      ,sh_render :: Render_f st
                      ,sh_key :: Key_f st
                      ,sh_delay :: Int -- ^ Frame delay in ms
                      ,sh_height :: Int -- ^ Window height
                      }

sh_bracket_nil :: Bracket_f st
sh_bracket_nil =
    let no_op _ _ st = return st
    in (no_op,no_op)

sh_sc3_nil :: Sc3_f st
sh_sc3_nil _ _ st = return st

sh_monitor_bus_syn :: Int -> Synthdef
sh_monitor_bus_syn nc =
    let i = in' nc ar (control kr "bus" 0)
        r = recordBuf ar (control kr "bufnum" 0) 0 1 0 1 Loop 1 DoNothing i
        nm = "sh_monitor_bus_" ++ show nc
    in synthdef nm r

sh_monitor_bus :: Transport t => (Int,Int) -> Shell st -> t -> IO ()
sh_monitor_bus (gr,ix) sh fd = do
  let b = fromIntegral (sh_b sh)
      ix' = fromIntegral ix
      syn = sh_monitor_bus_syn (sh_nc sh)
  _ <- async fd (d_recv syn)
  _ <- async fd (b_alloc (sh_b sh) (sh_nf sh) (sh_nc sh))
  sendMessage fd (g_new [(gr,AddToTail,0)])
  sendMessage fd (s_new (synthdefName syn) (-1) AddToTail gr [("bufnum",b),("bus",ix')])

deinterleave :: Int -> [a] -> [[a]]
deinterleave nc l =
    case nc of
      1 -> [l]
      _ -> transpose (chunksOf nc l)

sh_std_sc3 :: Transport t => Shell st -> t -> IO (Shell st)
sh_std_sc3 sh fd = do
  let b = sh_b sh
      nf = sh_nf sh
      nc = sh_nc sh
  sendMessage fd (b_getn b [(0,nf * nc)])
  r <- waitDatum fd "/b_setn"
  let d = case r of
            _:Int32 0:Int32 _:xs -> mapMaybe datum_floating xs
            _ -> []
  return (sh {sh_data = deinterleave nc d})

sh_on_close :: Udp -> Shell b -> b -> IO b
sh_on_close fd sh st = do
  let (_,c_f) = sh_bracket sh
  sendMessage fd (n_free [3])
  c_f fd sh st

sh_keypress_f :: G.WidgetClass w => w -> IORef st -> Udp -> Shell st -> (G.KeyVal, T.Text) -> IO Bool
sh_keypress_f w r fd sh (kv,nm) = do
  case T.unpack nm of
    "Escape" -> do st <- readIORef r
                   _ <- sh_on_close fd sh st
                   G.widgetDestroy w
    _ -> case G.keyToChar kv of
           Just c -> modifyIORef r (sh_key sh c)
           _ -> return ()
  return True

sh_update_f :: G.WidgetClass w => w -> C.Surface -> IORef st -> Udp -> Shell st -> IO Bool
sh_update_f c s r fd sh = do
  Just w <- G.widgetGetWindow c
  bd <- readIORef r
  sh' <- sh_std_sc3 sh fd
  bd' <- sh_sc3 sh' fd sh' bd >>= C.renderWith s . sh_render sh' sh'
  C.surfaceFlush s
  _ <- G.renderWithDrawWindow w (C.setSourceSurface s 0 0 >> C.paint)
  writeIORef r bd'
  return True

sc3_fd :: IO Udp
sc3_fd = openUdp "127.0.0.1" 57110

shell :: Shell st -> st -> IO ()
shell sh i_st = do
  let nf = sh_nf sh
      nf' = fromIntegral nf
      h = sh_height sh
      h' = fromIntegral h
      (i_f,_) = sh_bracket sh
  fd <- sc3_fd
  st <- i_f fd sh i_st
  case sh_mon sh of
    Just mon -> sh_monitor_bus mon sh fd
    Nothing -> return ()
  r <- newIORef st
  s <- C.createImageSurface C.FormatARGB32 nf h
  C.renderWith s (do C.rectangle 0 0 nf' h'
                     C.setSourceRGBA 0 0 0 1
                     C.fill)
  _ <- G.initGUI
  w <- G.windowNew
  c <- G.drawingAreaNew
  G.set w [G.windowTitle G.:= "hsc3"
          ,G.windowResizable G.:= False]
  G.widgetSetSizeRequest w nf (sh_height sh)
  _ <- G.on w G.keyPressEvent (do kv <- G.eventKeyVal
                                  nm <- G.eventKeyName
                                  liftIO (sh_keypress_f w r fd sh (kv,nm)))
  _ <- G.on w G.destroyEvent (liftIO (G.mainQuit >> return True))
  _ <- G.on c G.exposeEvent (liftIO (sh_update_f c s r fd sh))
  _ <- G.timeoutAdd (G.widgetQueueDraw w >> return True) (sh_delay sh)
  G.set w [G.containerChild G.:= c]
  G.widgetShowAll w
  G.mainGUI

sh_default_mon :: Int -> Render_f st -> Key_f st -> Shell st
sh_default_mon nc r k = Shell (Just (3,0)) nc 512 10 [] sh_bracket_nil sh_sc3_nil r k 15 512

sh_default_buf :: (Int, Int, Int) -> Render_f st -> Key_f st -> Shell st
sh_default_buf (b,nc,nf) r k = Shell Nothing nc nf b [] sh_bracket_nil sh_sc3_nil r k 25 512
