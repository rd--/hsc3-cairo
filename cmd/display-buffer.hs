import qualified Graphics.Rendering.Cairo as C {- cairo -}
import System.Environment {- base -}

import Sound.SC3 {- hsc3 -}
import Sound.SC3.Cairo.Scope.Shell {- hsc3-cairo -}

-- * Type

type R = Double
data BD = BD {bd_left :: R,bd_right :: R}

render_line :: R -> (R,R) -> [R] -> C.Render ()
render_line h (l,r) d = do
  let f (x,y) = let y' = linlin_hs (l,r) (0,h) (r - y)
                in if x == 0 then C.moveTo x y' else C.lineTo x y'
  mapM_ f (zip [0..] d)
  C.stroke

bd_render :: Render_F BD
bd_render sh bd = do
  let nf = fromIntegral (sh_nf sh)
      h = fromIntegral (sh_height sh)
  C.rectangle 0 0 nf h
  C.setSourceRGBA 0 0 0 1
  C.fill
  C.setSourceRGBA 0 1 0 1
  let f = render_line h (bd_left bd,bd_right bd)
      g d = C.save >> f d >> C.restore
  mapM_ g (sh_data sh)
  return bd

bd_key :: Key_F BD
bd_key = const id

-- > display_buffer 512 12 (0,12)
display_buffer :: Int -> Int -> (R,R) -> IO ()
display_buffer h b (l,r) = do
  (_,nf,nc,_) <- withSC3 (b_query1_unpack b)
  let sh = sh_default_buf (b,nc,nf) bd_render bd_key
  shell (sh {sh_height = h}) (BD l r)

main :: IO ()
main = do
  a <- getArgs
  case a of
    [h,b,l,r] -> display_buffer (read h) (read b) (read l,read r)
    _ -> putStrLn "display-buffer height:int ix:int left:float right:float"
