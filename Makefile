all:
	echo "hsc3-cairo"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hsc3-cairo
